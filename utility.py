def remove_char_list(text, chars):
    return ''.join([c for c in text if c not in chars])


def remove_string_list(text, strings):
    for string in strings:
        text = text.replace(string, "")
    return text
