import csv
import requests
from bs4 import BeautifulSoup
import utility


def scrape(page_num, categoria):
    dados = []
    for i in range(1, page_num + 1):
        dados.append(_scrape_url(f'https://www.turbosquid.com/3d-model/{categoria}?page_num={i}'))
    return dados


def _scrape_url(url):
    page0 = requests.get(url)
    soup0 = BeautifulSoup(page0.text, 'html.parser')
    items = soup0.find_all('div', class_="search-lab AssetTile-md tile-large")

    lista_items = []
    for item in items:
        name = item.find(class_="asset_name_item icnCertification").get_text().strip()
        url = item.find(class_="AssetInner").find(class_="asset_mouseover_item").find('a')['href']
        preco = item.find(class_="lightPrice").get_text().strip()

        page1 = requests.get(url)
        soup1 = BeautifulSoup(page1.text, 'html.parser')
        polygons = ''.join(soup1.find(id="FPSpec_polygons").get_text().strip().split())
        vertices = ''.join(soup1.find(id="FPSpec_vertices").get_text().strip().split())

        preco = preco.replace('$', '').replace(',', '.')
        polygons = utility.remove_string_list(polygons, ['Polygons', '"']).replace(',', '').strip()
        vertices = utility.remove_string_list(vertices, ['Vertices', '"']).replace(',', '').strip()

        novoItem = [name, preco, vertices, polygons, url]

        lista_items.append(novoItem)

        break

    return lista_items


################################################################################################

# definindo as categorias do turbosquid (faltando algumas)
categorias = [
    'airplane',
    'anatomy',
    'architecture',
    'animal',
    'car',
    'character',
    'food-and-drink',
    'furnishings',
    'industrial',
    'interior-design',
    'man',
    'nature',
    'people',
    'robot',
    'technology',
    'trees',
    'vehicle',
    'woman',
]

# escolher qual categoria
while True:
    print(f'Escolha a categoria:')

    for cat in categorias:
        print(cat, end=' - ')
    print('\n')

    categoriaEscolhida = str(input(">> ")).strip()

    if categoriaEscolhida not in categorias:
        print("Categoria não existe. Tente novamente.")
    else:
        break

# fazer o scrape
perPageData = scrape(1, categoriaEscolhida)

# salvar para .csv
try:
    with open(f'turbosquid_{categoriaEscolhida}.csv', 'w', newline='') as file:
        csvFile = csv.writer(file, delimiter=',')
        csvFile.writerow(['Nome', 'Preco($)', 'Vertices', 'Poligonos', 'URL'])
        for items in perPageData:
            for item in items:
                csvFile.writerow(item)
except IOError:
    print("Erro ao criar arquivo.")
